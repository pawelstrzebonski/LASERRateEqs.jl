# Example

A fundamental aspect of the rate equation solving
is the parameter definitions. These are stored
as a dictionary and passed to functions. By
default a set of "typical" parameters for a
VCSEL will be used. The parameters should
use `Unitful` units:

```Julia
# Import the package
import LASERRateEqs
# Import units package
using Unitful
# Load default parameters
Q = copy(LASERRateEqs.VCSELQuants)
# We can modify the values if need be
```

We can simulate the dynamics given a modulated
current and/or confinement factor. There
are helper functions defined for certain modulated
current schemes. We define these as functions
that can be passed to the time-domain solvers:

```Julia
# We will only have a single laser cavity
Ncav = 1
# And a single mode
Nmode = 1
# Define a sinusoidal driving current
IDC = 1u"mA"
IAC = 0.1u"mA"
freq = 1u"GHz"
I(t) = LASERRateEqs.Isin(t, IDC, IAC, Ncav, freq)
# And a constant confinement factor
Gamma(t) = ones(Ncav, Nmode) * Q[:Gamma_xy]*Q[:Gamma_z]
```

We solve for the carrier/photon populations
as a function of time from time `0` to `tmax`:

```Julia
# Evaluate for the first 100 ns
tmax = 100u"ns"
sol = LASERRateEqs.laser_array_time_simulation(Ncav, Nmode, tmax, I, Gamma)
```

The result will be a `OrdinaryDiffEq` solution
that evaluates a vector of carrier and photon
counts.

If we only care about the steady-state solutions
given a constant driving current and confinement
factor, then we can use a helper function that
estimates the steady-state as result after
`tmax` time to stabilize:

```Julia
# Define the steady driving current and confinement factor
I = [1u"mA"]
Gamma = ones(Ncav, Nmode) * Q[:Gamma_xy]*Q[:Gamma_z]
# And calculate the carrier/photons after tmax
N, P = LASERRateEqs.laser_array_steady_state(Ncav, Nmode, tmax, I, Gamma);
```

The results are the vectors for the carrier and
photon counts.

If we want to model the photon-photon
resonance effect on the small-signal modulation
response, we must first estimate the effective
confinement factor modulation. This quantity
can be calculated using a variety of methods.
First is a full numeric integration approach:

```Julia
# Photon-photon resonance frequency
f_ppr = 10u"GHz"
# Define a time-varying confinement factor
Gamma(t) = ones(Ncav, Nmode) * (0.5 + 0.1 * sin(t * f_ppr * 2 * pi))
# Modulation frequency
f_mod = 1u"GHz"
# Lifetime of (quasi-)phase-locking
t_phaselock = 1.5u"ms"
# Estimated confinement modulation factor
dGamma = LASERRateEqs.deltaGamma(Gamma, f_mod * 2 * pi, t_phaselock)
```

Alternatively, if we know that our confinement
factor varies sinusoidally, we can use a much
fastor calculation using the analytical estimate
of the integral:

```Julia
# Amplitude of confinement factor variation in time domain
dg = ones(Ncav, Nmode) * 0.1
dGamma = LASERRateEqs.deltaGammaSin(f_ppr * 2 * pi, f_mod * 2 * pi, t_phaselock, dg)
```

With this in hand (or if we want to neglect
PPR, then we just set `dGamma` to be
zero) and the steady-state carrier and photon
counts `N` and `P`, we can calculate the small-signal
response at a given frequency:

```Julia
# Steady-state (or average) confinement factor
Gamma = ones(Ncav, Nmode) * Q[:Gamma_xy]*Q[:Gamma_z]
# Coefficients of injection current modulation
dI = 0.1u"mA" * ones(Ncav)
# Calculate coefficients of carrier and photon response
dN, dP = LASERRateEqs.small_signal_modulation_response(N, P, Gamma, dI, dGamma, 2 * pi * f_mod);

```
