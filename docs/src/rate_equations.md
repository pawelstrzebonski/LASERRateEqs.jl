# rate_equations.jl

## Description

This file implements the time-domain laser rate (differential) equations.

## Functions

```@autodocs
Modules = [LASERRateEqs]
Pages   = ["rate_equations.jl"]
```
