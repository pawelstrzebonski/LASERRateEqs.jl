# References

The theory and methods implemented in this package are based on a variety
of sources. This is due to the need to adapt and extend existing theory
and equations for laser rate equations and small-signal analysis that
is capable of analyzing multi-cavity and multi-(array)-mode systems,
as well as model photon-photon resonance (PPR) effects is a somewhat
intuitive and generally applicable way.

The most fundamental source for laser rate equations and small-signal
analysis is Chapter 5 of
["Diode Lasers and Photonic Integrated Circuits"](https://coldren.ece.ucsb.edu/book).
It is a good source for single- and multi-mode equations and theory,
small-signal analysis math and theory, and of common parameter for
those equations.

[Laakso, A. and M. Dumitrescu. "Modified rate equation model including
the photon–photon resonance." Optical and Quantum Electronics 42, 2010,
785-791.](https://doi.org/10.1007/S11082-011-9483-6)
incorporates PPR into this analysis by determining a driving confinement
factor term for the small-signal analysis based on the the field interaction
of a pair of modes and combining the two interacting modes into a single
mode in the subsequent analysis.

This package implements/extends the equations in those works to support
multiple array modes that extend across multiple cavities via a confinement
factor matrix that expresses the overlap between each individual mode and
each individual cavity. References to support this way of dealing with
multiple modes and cavities should be found and added.

The PPR effect is analyzed via time-varying confinement factor, similar
to Laakso et al. It is presumed that PPR effects in VCSELs can be modeled
via a similar methods that calculates the time-varying transverse
confinement factor (as opposed to the longitudinal confinement factor
analyzed in Laakso's DFB analysis). The calculation of the effective
confinement factor driving term for the small-signal analysis is
derived from Laakso (the exact equation is not explicitly given itself but
is incorporated into the small-signal modulation response equation).
Additional, more detailed, sources for such an analysis should be found and added.
