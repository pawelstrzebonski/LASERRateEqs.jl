# rate\_equation\_steady\_state.jl

## Description

This file implements a utility function trying to calculate the time-domain
rate equations until they converge to a steady-state in order to determine
this steady-state.

## Functions

```@autodocs
Modules = [LASERRateEqs]
Pages   = ["rate_equation_steady_state.jl"]
```
