# modulation_functions.jl

## Description

This file implements some basic modulated driving current functions.

## Functions

```@autodocs
Modules = [LASERRateEqs]
Pages   = ["modulation_functions.jl"]
```
