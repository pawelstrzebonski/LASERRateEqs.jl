# Contributing and Development

Contributions to this package are welcomed. Please submit issues/pull-requests
on the GitLab project.

## TODOs

The following are some areas for development and improvement:

* More, better unit tests and documentation
* Optimizations
* Examples of usage
* More default equation parameters (or better ways of updating those parameters)
* More/better references for the theory and math

## Guidance

When adding functionality or making modifications, please keep in mind
the following:

* Avoid importing too large/many dependencies (especially for differential equation solving)
* Keep `Unitful` quantities for analysis to avoid units related errors
* We aim to minimize the amount of code and function repetition
