# photon\_photon\_resonance.jl

## Description

This file implements various equations for determining the effective
confinement factor driving term used for small-signal analysis.

## Functions

```@autodocs
Modules = [LASERRateEqs]
Pages   = ["photon_photon_resonance.jl"]
```
