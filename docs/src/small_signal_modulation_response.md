# small\_signal\_modulation\_response.jl

## Description

This file implements the frequency-domain small-signal analysis matrix
equations and solves them.

## Functions

```@autodocs
Modules = [LASERRateEqs]
Pages   = ["small_signal_modulation_response.jl"]
```
