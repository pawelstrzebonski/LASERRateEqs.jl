# LASERRateEqs.jl Documentation

## About

`LASERRateEqs.jl` is a Julia package for analyzing lasers and laser arrays
using time-domain photon/carrier rate equations and derived small-signal
analysis.

Note: This is a work-in-progress and may have errors!

## Installation

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/LASERRateEqs.jl
```

## Features

* Time-domain rate equation solving
* Frequency-domain small-signal solving
* Supports multiple modes
* Supports multiple cavities
* Supports photon-photon resonance as time-varying confinement factor
* Most calculations use integrated units (for consistency and avoiding units errors)
