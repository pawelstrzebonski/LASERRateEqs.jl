# quantities.jl

## Description

This file provides default laser parameters used for analysis.

## Functions

```@autodocs
Modules = [LASERRateEqs]
Pages   = ["quantities.jl"]
```
