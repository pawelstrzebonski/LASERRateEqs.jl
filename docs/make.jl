using Documenter
import LASERRateEqs

makedocs(
    sitename = "LASERRateEqs",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "LASERRateEqs.jl"),
    pages = [
        "Home" => "index.md",
        "Examples" => "example.md",
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => [
            "modulation_functions.jl" => "modulation_functions.md",
            "photon\\_photon\\_resonance.jl" => "photon_photon_resonance.md",
            "quantities.jl" => "quantities.md",
            "rate\\_equations.jl" => "rate_equations.md",
            "rate\\_equation\\_steady\\_state.jl" => "rate_equation_steady_state.md",
            "rate\\_equation\\_time\\_domain.jl" => "rate_equation_time_domain.md",
            "small\\_signal\\_modulation\\_response.jl" =>
                "small_signal_modulation_response.md",
        ],
        "test/" => [
            "modulation\\_functions.jl" => "modulation_functions_test.md",
            "photon\\_photon\\_resonance.jl" => "photon_photon_resonance_test.md",
            "quantities.jl" => "quantities_test.md",
            "rate\\_equations.jl" => "rate_equations_test.md",
            "rate\\_equation\\_steady\\_state.jl" =>
                "rate_equation_steady_state_test.md",
            "rate\\_equation\\_time\\_domain.jl" => "rate_equation_time_domain_test.md",
            "small\\_signal\\_modulation\\_response.jl" =>
                "small_signal_modulation_response_test.md",
        ],
    ],
)
