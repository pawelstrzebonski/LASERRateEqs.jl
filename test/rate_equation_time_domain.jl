@testset "rate_equation_time_domain.jl" begin
    tmax = 10u"ns"
    f = 1u"GHz"
    Ncav = 1
    Nmode = 3
    I1(t) = LASERRateEqs.Isin(t, 1u"mA", 0.1u"mA", Ncav, f)
    Gamma1(t) = ones(Ncav, Nmode) ./ Ncav

    res = 0
    # Densities should have increased over time
    @test (res = LASERRateEqs.laser_array_time_simulation(Ncav, Nmode, tmax, I1, Gamma1);
    true)
    @test all(res(ustrip(u"s", tmax)) .> res(0))
    Ncav = 2
    Nmode = 3
    I2(t) = LASERRateEqs.Isin(t, 1u"mA", 0.1u"mA", Ncav, f)
    Gamma2(t) = ones(Ncav, Nmode) ./ Ncav
    @test (res = LASERRateEqs.laser_array_time_simulation(Ncav, Nmode, tmax, I2, Gamma2);
    true)
    @test all(res(ustrip(u"s", tmax)) .> res(0))
end
