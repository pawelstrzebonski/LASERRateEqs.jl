@testset "photon_photon_resonance.jl" begin
    f_mod = 1u"Hz"
    t_phaselocked = 1.5u"s"
    f_ppr = 10u"Hz"
    Gamma(t) = 0.5 + 0.1 * sin(t * f_ppr * 2 * pi)

    res = 0
    # When t_phaselocked is not a comensurate to the PPR frequency, we have non-zero value
    @test (res = LASERRateEqs.deltaGamma(Gamma, f_mod * 2 * pi, t_phaselocked); true)
    @test abs(res) > 0
    # When t_phaselocked is comensurate to the PPR frequency, we have zero value
    @test (res = LASERRateEqs.deltaGamma(Gamma, f_mod * 2 * pi, 1u"s"); true)
    @test abs(res) < 1e-9
    # Value is much greater (maximal) at PPR frequency
    @test abs(LASERRateEqs.deltaGamma(Gamma, f_mod * 2 * pi, t_phaselocked)) <
          abs(LASERRateEqs.deltaGamma(Gamma, f_ppr * 2 * pi, t_phaselocked))

    omega_mod = 2 * pi * 1u"Hz"
    omega_ppr = 2 * pi * 10u"Hz"
    dg = 0.1
    # When t_phaselocked is not a comensurate to the PPR frequency, we have non-zero value
    @test (res = LASERRateEqs.deltaGammaSin(omega_ppr, omega_mod, t_phaselocked, dg); true)
    # When t_phaselocked is comensurate to the PPR frequency, we have zero value
    @test (res = LASERRateEqs.deltaGammaSin(omega_ppr, omega_mod, 1u"s", dg); true)
    @test abs(res) < 1e-9
    @test abs(LASERRateEqs.deltaGammaSin(omega_ppr, omega_mod, t_phaselocked, dg)) < abs(
        LASERRateEqs.deltaGammaSin(
            omega_ppr,
            omega_ppr + eps(omega_ppr),
            t_phaselocked,
            dg,
        ),
    )
end
