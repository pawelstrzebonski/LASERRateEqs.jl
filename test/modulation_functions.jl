@testset "modulation_functions.jl" begin
    I = 0
    f = 1u"Hz"
    t = (0:0.01:1) .* 1u"s"

    # Binary functions should return +/-1
    @test (I = LASERRateEqs.squarewave.(t, f); true)
    @test all(@. isone(I) | isone(-I))
    @test (I = LASERRateEqs.randombitstream.(t .* 10, f); true)
    @test all(@. isone(I) | isone(-I))

    # Currents within bounds, average is approximately Idc
    Idc, Iac = 1u"mA", 0.1u"mA"
    @test (I = getindex.(LASERRateEqs.Isin.(t, Idc, Iac, 1, f), 1); true)
    @test all(@. (Idc - Iac) <= I <= (Idc + Iac))
    @test Idc * 0.99 <= sum(I) / length(I) <= Idc * 1.01
    @test (I = getindex.(LASERRateEqs.Isqw.(t, Idc, Iac, 1, f), 1); true)
    @test all(@. (Idc - Iac) <= I <= (Idc + Iac))
    @test Idc * 0.99 <= sum(I) / length(I) <= Idc * 1.01
    @test (I = getindex.(LASERRateEqs.Ibs.(t .* 10, Idc, Iac, 1, f), 1); true)
    @test all(@. (Idc - Iac) <= I <= (Idc + Iac))
    @test Idc * 0.9 <= sum(I) / length(I) <= Idc * 1.1
end
