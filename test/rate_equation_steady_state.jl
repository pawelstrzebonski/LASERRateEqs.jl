@testset "rate_equation_steady_state.jl" begin
    res = 0

    # Test results to values in Table 5.1 of Coldren and Corzine
    tmax = 10u"ns"
    Ncav = 1
    Nmode = 1
    I = 2.32u"mA" * ones(Ncav)
    Gamma0 = LASERRateEqs.VCSELQuants[:Gamma_xy] * LASERRateEqs.VCSELQuants[:Gamma_z]
    Gamma = [Gamma0][:, :]
    @test (res = LASERRateEqs.laser_array_steady_state(Ncav, Nmode, tmax, I, Gamma);
    true)
    @test approxeq(
        ustrip(u"cm^-3", res[1][1] / LASERRateEqs.VCSELQuants[:V]),
        3.93e18,
        rtol = 1e-2,
    )
    @test approxeq(
        ustrip(u"cm^-3", res[2][1] * Gamma[1] / LASERRateEqs.VCSELQuants[:V]),
        2.80e14,
        rtol = 1e-2,
    )

    # Adding another mode of lower Gamma should not change carriers and first mode by much, second mode should be low power
    res2 = 0
    Nmode = 2
    Gamma = [Gamma0; Gamma0 ./ 2]'
    @test (res2 = LASERRateEqs.laser_array_steady_state(Ncav, Nmode, tmax, I, Gamma);
    true)
    @test approxeq(res[1][1], res2[1][1], rtol = 1e-3)
    @test approxeq(res[2][1], res2[2][1], rtol = 1e-3)
    @test res2[2][1] / res2[2][2] > 1e3

    # Adding another cavity and conserving Gamma should not change carriers and first mode power should be doubled
    res2 = 0
    Nmode = 1
    Ncav = 2
    Gamma = [Gamma0 / 2 Gamma0 / 2]'
    I = 2.32u"mA" * ones(Ncav)
    @test (res2 = LASERRateEqs.laser_array_steady_state(Ncav, Nmode, tmax, I, Gamma);
    true)
    @test approxeq(res[1][1], res2[1][1], rtol = 1e-3)
    @test approxeq(res2[1][1], res2[1][2], rtol = 1e-3)
    @test approxeq(res[2][1], res2[2][1] / 2, rtol = 1e-3)
end
