@testset "small_signal_modulation_response.jl" begin
    Q = LASERRateEqs.VCSELQuants
    tmax = 30u"ns"
    omega = 1u"GHz"
    Ncav = 1
    Nmode = 1
    I = 2.32u"mA" * ones(Ncav)
    dI = 0.1u"mA" * ones(Ncav)
    Gamma0 = Q[:Gamma_xy] * Q[:Gamma_z]
    Gamma = [Gamma0][:, :]
    dGamma = [0][:, :]

    res = 0

    # Compare values to those in Table 5.1 of Coldren and Corzine
    N, P = LASERRateEqs.laser_array_steady_state(Ncav, Nmode, tmax, I, Gamma)
    @test approxeq(
        ustrip(u"Hz", LASERRateEqs.gammaNN(N, P, Gamma, 1, 1, Q)),
        1.67e9,
        rtol = 1e-2,
    )
    @test approxeq(
        ustrip(u"Hz", LASERRateEqs.gammaNP(N, P, Gamma, 1, 1, Q)) / Gamma[1],
        1.18e13,
        rtol = 1e-2,
    )
    @test approxeq(
        ustrip(u"Hz", LASERRateEqs.gammaPN(N, P, Gamma, 1, 1, Q, Coldren = true)) *
        Gamma[1],
        3.88e7,
        rtol = 1e-2,
    )
    #TODO: This is off by a bit more than the others. Why?
    @test approxeq(
        ustrip(u"Hz", LASERRateEqs.gammaPP(N, P, Gamma, 1, 1, Q, Coldren = true)),
        1.93e9,
        rtol = 3e-2,
    )
    @test (
        res = LASERRateEqs.small_signal_modulation_response(N, P, Gamma, dI, dGamma, omega);
        true
    )

    Ncav = 2
    Nmode = 3
    I = 1u"mA" * ones(Ncav)
    dI = 0.1u"mA" * ones(Ncav)
    Gamma = ones(Ncav, Nmode) ./ Ncav
    dGamma = 1e-10 .* ones(Ncav, Nmode) ./ Ncav
    N, P = LASERRateEqs.laser_array_steady_state(Ncav, Nmode, tmax, I, Gamma)
    @test (
        res = LASERRateEqs.small_signal_modulation_response(N, P, Gamma, dI, dGamma, omega);
        true
    )
end
