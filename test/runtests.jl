import LASERRateEqs
using LASERRateEqs.Unitful
import Test: @test_broken, @test, @test_throws, @testset

tests = [
    "quantities",
    "rate_equations",
    "modulation_functions",
    "photon_photon_resonance",
    "rate_equation_time_domain",
    "rate_equation_steady_state",
    "small_signal_modulation_response",
]

approxeq(a, b; rtol = 1e-4, atol = 0) = all(isapprox.(a, b, rtol = rtol, atol = atol))

for t in tests
    @info "Running " * t * ".jl"
    include("$(t).jl")
    @info "Finished " * t * ".jl"
end
