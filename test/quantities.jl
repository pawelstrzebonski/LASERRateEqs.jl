@testset "quantities.jl" begin
    Q = nothing

    @test (Q = copy(LASERRateEqs.VCSELQuants); true)
    @test (Q[:eta_i] = 0.7; true)
end
