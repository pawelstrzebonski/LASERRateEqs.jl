@testset "rate_equations.jl" begin
    Q = copy(LASERRateEqs.VCSELQuants)

    P = [1]
    N = Q[:N_tr] .* Q[:V]

    # Zero gain at transparency or lower carrier density
    @test iszero(LASERRateEqs.Gfun.(N ./ Q[:V], P ./ Q[:V], Ref(Q)))

    # Positive gain above transparency density
    @test all(LASERRateEqs.Gfun.(2 .* N ./ Q[:V], P ./ Q[:V], Ref(Q)) .> 0u"cm^-1")

    # Same for 2 cavity calculation
    N = [Q[:N_tr], 2 * Q[:N_tr]] .* Q[:V]
    G = LASERRateEqs.Gfun.(N ./ Q[:V], [P; P] ./ Q[:V], Ref(Q))
    @test iszero(G[1]) && G[2] > 0u"cm^-1"

    # Increasing photon density compresses (lowers) gain
    N = 2 * Q[:N_tr] .* Q[:V]
    @test all(
        LASERRateEqs.Gfun.(N ./ Q[:V], P ./ Q[:V], Ref(Q)) .>
        LASERRateEqs.Gfun.(N ./ Q[:V], P ./ Q[:V] .* 1e6, Ref(Q)),
    )

    P = [1]
    N = [Q[:N_tr] / 2, 2 * Q[:N_tr]] .* Q[:V]
    t = 0u"s"
    I(t) = [1, 1] .* 1u"mA"
    Gamma(t) = [0.5, 0.5]
    res = 0

    # Carriers increase
    @test (res = [LASERRateEqs.dNdt(N, P, i, t, I, Gamma, Q) for i = 1:2]; true)
    @test res[1] > 0u"s^-1" && res[2] > 0u"s^-1"

    # Above transparency photons increase
    @test (res = LASERRateEqs.dPdt(N, P, 1, t, Gamma, Q); true)
    @test res > 0u"s^-1"

    P = [1e10]
    N = [Q[:N_tr], Q[:N_tr]] .* Q[:V]
    # At transparency photons decrease at high photon density
    @test (res = LASERRateEqs.dPdt(N, P, 1, t, Gamma, Q); true)
    @test res < 0u"s^-1"
end
