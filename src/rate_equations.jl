using Unitful

# Calculate the gain as a function of carrier and photon density
# Source: Coldren page 258, Eq 5.29
"""
    Gfun(N::Quantity, Pcav::Quantity, Q::Dict)

Calculate the gain in each cavity, subject to gain compression.
"""
function Gfun(N::Quantity, Pcav::Quantity, Q::Dict)
    N > Q[:N_tr] ?
    Q[:g_0N] / (1 + Q[:epsilon] * Pcav) * log((N + Q[:N_s]) / (Q[:N_tr] + Q[:N_s])) :
    zero(Q[:g_0N])
end

# Source: Coldren page 249, Eq 5.3
"""
    dNdt(
		N::AbstractVector,
		P::AbstractVector,
		icav::Integer,
		t::Quantity,
		I::Function,
		Gamma::Function,
		Q::Dict,
	)

Time-change in carriers.

# Arguments:
- `N`: carriers in cavities.
- `P`: photons in modes.
- `icav`: index of cavity to calculate for.
- `t`: simulation time.
- `I`: driving currents to each cavity (function of time that returns vector of current values).
- `Gamma`: modal gain-overlap factor (function of time that returns matrix giving overlap of each [Cavity, Mode]).
- `Q`: dictionary of various laser constants to be used in laser equations.
"""
function dNdt(
    N::AbstractVector,
    P::AbstractVector,
    icav::Integer,
    t::Quantity,
    I::Function,
    Gamma::Function,
    Q::Dict,
)
    Nmode = length(P)
    V = typeof(Q[:V]) <: AbstractVector ? Q[:V][icav] : Q[:V]
    # Calculate photons in cavity
    Pcav = sum(Gamma(t)[icav, :] .* P)
    g = Gfun(N[icav] / V, Pcav / V, Q)
    tau_st = N[icav] / (Q[:v_g] * g * Pcav)
    # Current in - Non-radiative recombination - Radiative recombination
    Q[:eta_i] * I(t)[icav] / Q[:q] - N[icav] / Q[:tau] -
    g * sum(@. Q[:v_g] * Gamma(t)[icav, :] * P)
end

# Source: Coldren page 249, Eq 5.4
"""
    dPdt(
		N::AbstractVector,
		P::AbstractVector,
		imode::Integer,
		t::Quantity,
		Gamma::Function,
		Q::Dict,
	)

Time-change in photons.

# Arguments:
- `N`: Carriers in cavities.
- `P`: photons in modes.
- `imode`: index of the mode to calculate for.
- `t`: simulation time.
- `Gamma`: modal gain-overlap factor (function of time that returns matrix giving overlap of each [Cavity, Mode]).
- `Q`: dictionary of various laser constants to be used in laser equations.
"""
function dPdt(
    N::AbstractVector,
    P::AbstractVector,
    imode::Integer,
    t::Quantity,
    Gamma::Function,
    Q::Dict,
)
    Ncav = length(N)
    Nmode = length(P)
    V = typeof(Q[:V]) <: AbstractVector ? Q[:V] : fill(Q[:V], Ncav)
    # Calculate photons in cavities
    Pcav = [sum(Gamma(t)[icav, :] .* P) for icav = 1:Ncav]
    (
        Q[:v_g] * sum(Gamma(t)[:, imode] .* Gfun.(N ./ V, Pcav ./ V, Ref(Q))) -
        1 / Q[:tau_p]
    ) * P[imode] + sum(N ./ Q[:tau_nprime])
end
