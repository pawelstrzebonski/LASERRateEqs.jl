import OrdinaryDiffEq
using Unitful

"""
    rateq!(du::AbstractVector, u::AbstractVector, p::AbstractVector, t::Number)

Laser rate equation update function intended for `OrdinaryDiffEq`.
"""
function rateq!(du::AbstractVector, u::AbstractVector, p::AbstractVector, t::Number)
    Ncav, Nmode, I, Gamma, Q = p
    N = u[1:Ncav]
    P = u[Ncav.+(1:Nmode)]
    dN = [dNdt(N, P, i, t * 1u"s", I, Gamma, Q) for i = 1:Ncav]
    dP = [dPdt(N, P, i, t * 1u"s", Gamma, Q) for i = 1:Nmode]
    du[1:Ncav] = Unitful.ustrip.(u"s^-1", dN)
    du[Ncav.+(1:Nmode)] = Unitful.ustrip.(u"s^-1", dP)
end

"""
    laser_array_time_simulation(
		Ncav::Integer,
		Nmode::Integer,
		tmax::Quantity,
		I::Function,
		Gamma::Function;
		laserconstants::Dict = VCSELQuants,
	)

Simulate the evolution of carrier and photon counts as a function of
time. Returns an `OrdinaryDiffEq` solution for cavity carrier and
modal photon counts `[N; P]` as a function of time.

# Arguments:
- `Ncav`: number of cavities.
- `Nmode`: number of array modes.
- `tmax`: time of simulation.
- `I`: driving currents to each cavity (function of time that returns vector of current values).
- `Gamma`: modal gain-overlap factor (function of time that returns matrix giving overlap of each [Cavity, Mode]).
- `laserconstants`: dictionary of various laser constants to be used in laser equations.
"""
function laser_array_time_simulation(
    Ncav::Integer,
    Nmode::Integer,
    tmax::Quantity,
    I::Function,
    Gamma::Function;
    laserconstants::Dict = VCSELQuants,
)
    p = [Ncav, Nmode, I, Gamma, laserconstants]
    N0 = ones(Ncav)
    P0 = zeros(Nmode)
    u0 = [N0; P0]
    tspan = (0.0u"s", tmax)
    @assert size(I(tspan[1])) == (Ncav,)
    @assert size(Gamma(tspan[1])) == (Ncav, Nmode)
    prob = OrdinaryDiffEq.ODEProblem(rateq!, u0, Unitful.ustrip.(u"s", tspan), p)
    sol = OrdinaryDiffEq.solve(prob, OrdinaryDiffEq.Tsit5())
end
