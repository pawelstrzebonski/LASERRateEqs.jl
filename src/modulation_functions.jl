import Unitful
import Unitful: u, Quantity

# Length of random bit-stream (before repeating)
const Nbits = 10000
# Random bit-sequence
const bitstream = rand(Bool, Nbits)

"""
    squarewave(t::Quantity, f::Quantity)

Square-wave modulation function for frequency `f` at time `t` (returns
either `-1` or `+1`).
"""
squarewave(t::Quantity, f::Quantity) = Unitful.ustrip(u"Hz*s", t * f) % 1 > 0.5 ? 1 : -1

"""
    randombitstream(t::Quantity, f::Quantity)

Random bit-sequence modulation function for frequency `f` at time `t` (returns
either `-1` or `+1`).
"""
randombitstream(t::Quantity, f::Quantity) =
    bitstream[1+(round(Int, Unitful.ustrip(u"Hz*s", t * f))%Nbits)] ? 1 : -1

"""
    Isin(t::Quantity, IDC::Quantity, IAC::Quantity, Ncav::Integer, f::Quantity)

Sinusoidally modulated current.

# Arguments:
- `t`: time.
- `IDC`: DC component of current.
- `IAC`: AC component of current.
- `Ncav`: number of cavities being modulated.
- `f`: frequency.
"""
Isin(t::Quantity, IDC::Quantity, IAC::Quantity, Ncav::Integer, f::Quantity) =
    IDC * ones(Ncav) .+ IAC * sin(t * 2 * pi * f)

"""
    Isqw(t::Quantity, IDC::Quantity, IAC::Quantity, Ncav::Integer, f::Quantity)

Square-wave modulated current.

# Arguments:
- `t`: time.
- `IDC`: DC component of current.
- `IAC`: AC component of current.
- `Ncav`: number of cavities being modulated.
- `f`: frequency.
"""
Isqw(t::Quantity, IDC::Quantity, IAC::Quantity, Ncav::Integer, f::Quantity) =
    IDC * ones(Ncav) .+ IAC * squarewave(t, f)

"""
    Ibs(t::Quantity, IDC::Quantity, IAC::Quantity, Ncav::Integer, f::Quantity)

Random bit-sequence modulated current.

# Arguments:
- `t`: time.
- `IDC`: DC component of current.
- `IAC`: AC component of current.
- `Ncav`: number of cavities being modulated.
- `f`: frequency.
"""
Ibs(t::Quantity, IDC::Quantity, IAC::Quantity, Ncav::Integer, f::Quantity) =
    IDC * ones(Ncav) .+ IAC * randombitstream(t, f)
