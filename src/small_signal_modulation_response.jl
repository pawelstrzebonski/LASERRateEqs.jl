import LinearAlgebra
using Unitful

# Define the matrix terms
# Source: Coldren page 259, Eq 5.35
"""
    gammaNN(
		N::AbstractVector,
		P::AbstractVector,
		Gamma::AbstractMatrix,
		icav::Integer,
		imode::Integer,
		Q::Dict;
		Coldren::Bool = true,
	)

Carrier-carrier small-signal matrix equation term.

# Arguments:
- `N`: carrier numbers in cavities.
- `P`: photon numbers in modes.
- `Gamma`: modal gain-overlap factor (matrix giving overlap of each [Cavity, Mode]).
- `icav`: index of cavity to calculate for.
- `imode`: index of mode to calculate for.
- `Q`: dictionary of various laser constants to be used in laser equations.
- `Coldren`: whether to run in compatibility with assumptions in Coldren and Corzine book.
"""
function gammaNN(
    N::AbstractVector,
    P::AbstractVector,
    Gamma::AbstractMatrix,
    icav::Integer,
    imode::Integer,
    Q::Dict;
    Coldren::Bool = true,
)
    V = typeof(Q[:V]) <: AbstractVector ? Q[:V][icav] : Q[:V]
    Pcav = sum(Gamma[icav, :] .* P)
    1 / Q[:tau_DeltaN] + Q[:v_g] * Q[:a] / V * Pcav
end

"""
    gammaNP(
		N::AbstractVector,
		P::AbstractVector,
		Gamma::AbstractMatrix,
		icav::Integer,
		imode::Integer,
		Q::Dict;
		Coldren::Bool = true,
	)

Carrier-photon small-signal matrix equation term.

# Arguments:
- `N`: carrier numbers in cavities.
- `P`: photon numbers in modes.
- `Gamma`: modal gain-overlap factor (matrix giving overlap of each [Cavity, Mode]).
- `icav`: index of cavity to calculate for.
- `imode`: index of mode to calculate for.
- `Q`: dictionary of various laser constants to be used in laser equations.
- `Coldren`: whether to run in compatibility with assumptions in Coldren and Corzine book.
"""
function gammaNP(
    N::AbstractVector,
    P::AbstractVector,
    Gamma::AbstractMatrix,
    icav::Integer,
    imode::Integer,
    Q::Dict;
    Coldren::Bool = true,
)
    V = typeof(Q[:V]) <: AbstractVector ? Q[:V][icav] : Q[:V]
    1 / (Q[:tau_p]) - Q[:v_g] * P[imode] * Gamma[icav, imode]^2 * Q[:a_p] / V -
    (Coldren ? 0.0u"Hz" : Gamma[icav, imode] * Q[:R_spprime] * V / P[imode])
end

"""
    gammaPN(
		N::AbstractVector,
		P::AbstractVector,
		Gamma::AbstractMatrix,
		icav::Integer,
		imode::Integer,
		Q::Dict;
		Coldren::Bool = true,
	)

Photon-carrier small-signal matrix equation term.

# Arguments:
- `N`: carrier numbers in cavities.
- `P`: photon numbers in modes.
- `Gamma`: modal gain-overlap factor (matrix giving overlap of each [Cavity, Mode]).
- `icav`: index of cavity to calculate for.
- `imode`: index of mode to calculate for.
- `Q`: dictionary of various laser constants to be used in laser equations.
- `Coldren`: whether to run in compatibility with assumptions in Coldren and Corzine book.
"""
function gammaPN(
    N::AbstractVector,
    P::AbstractVector,
    Gamma::AbstractMatrix,
    icav::Integer,
    imode::Integer,
    Q::Dict;
    Coldren::Bool = true,
)
    V = typeof(Q[:V]) <: AbstractVector ? Q[:V][icav] : Q[:V]
    Q[:v_g] * P[imode] * Gamma[icav, imode] * Q[:a] / V +
    (Coldren ? 0.0u"Hz" : 1 / Q[:tau_DeltaNprime])
end

"""
    gammaPP(
		N::AbstractVector,
		P::AbstractVector,
		Gamma::AbstractMatrix,
		icav::Integer,
		imode::Integer,
		Q::Dict;
		Coldren::Bool = true,
	)

Photon-photon small-signal matrix equation term.

# Arguments:
- `N`: carrier numbers in cavities.
- `P`: photon numbers in modes.
- `Gamma`: modal gain-overlap factor (matrix giving overlap of each [Cavity, Mode]).
- `icav`: index of cavity to calculate for.
- `imode`: index of mode to calculate for.
- `Q`: dictionary of various laser constants to be used in laser equations.
- `Coldren`: whether to run in compatibility with assumptions in Coldren and Corzine book.
"""
function gammaPP(
    N::AbstractVector,
    P::AbstractVector,
    Gamma::AbstractMatrix,
    icav::Integer,
    imode::Integer,
    Q::Dict;
    Coldren::Bool = true,
)
    Pcav = [sum(Gamma[icav, :] .* P) for icav = 1:size(Gamma, 1)]
    g = Gfun.(N ./ Q[:V], Pcav ./ Q[:V], Ref(Q))
    Q[:v_g] * P[imode] * sum(Gamma[:, imode] .^ 2 .* Q[:a_p] ./ Q[:V]) +
    (Coldren ? 0u"Hz" : Q[:R_spprime] * sum(Gamma[:, imode] .* Q[:V]) / P[imode])
end

# Generate the cross-term matrix
# Source: Coldren page 259, Eq 5.37
"""
    matA(
		N::AbstractVector,
		P::AbstractVector,
		Gamma::AbstractMatrix,
		Q::Dict;
		Coldren::Bool = true,
	)

Generate the matrix for the small-signal modulation matrix equation.

# Arguments:
- `N`: carrier numbers in cavities.
- `P`: photon numbers in modes.
- `Gamma`: modal gain-overlap factor (matrix giving overlap of each [Cavity, Mode]).
- `Q`: dictionary of various laser constants to be used in laser equations.
- `Coldren`: whether to run in compatibility with assumptions in Coldren and Corzine book.
"""
function matA(
    N::AbstractVector,
    P::AbstractVector,
    Gamma::AbstractMatrix,
    Q::Dict;
    Coldren::Bool = true,
)
    Ncav, Nmode = size(Gamma)
    A = zeros(eltype(1.0u"Hz"), Ncav + Nmode, Ncav + Nmode)
    for icav = 1:Ncav
        A[icav, icav] = gammaNN(N, P, Gamma, icav, 0, Q)
    end
    for imode = 1:Nmode
        A[Ncav+imode, Ncav+imode] = gammaPP(N, P, Gamma, 0, imode, Q, Coldren = Coldren)
    end
    #TODO: gammaPP and gammaNN for cross-terms??? eq 17, 27 of Hamad et al
    for imode = 1:Nmode, icav = 1:Ncav
        A[icav, Ncav+imode] = gammaNP(N, P, Gamma, icav, imode, Q, Coldren = Coldren)
        A[Ncav+imode, icav] = -gammaPN(N, P, Gamma, icav, imode, Q, Coldren = Coldren)
    end
    A
end

# Generate the driving term vector
# Source: Coldren page 259, Eq 5.37
"""
    vecB(
		N::AbstractVector,
		P::AbstractVector,
		Gamma::AbstractMatrix,
		dI::AbstractVector,
		dGamma::AbstractMatrix,
		Q::Dict,
	)

Generate the vector for the small-signal modulation matrix equation.

# Arguments:
- `N`: carrier numbers in cavities.
- `P`: photon numbers in modes.
- `Gamma`: modal gain-overlap factor (matrix giving overlap of each [Cavity, Mode]).
- `dI`: strength of small-signal modulation in each cavity.
- `dGamma`: strength of photon-photon resonance induced gain-overlap variation (matrix giving overlap of each [Cavity, Mode]).
- `Q`: dictionary of various laser constants to be used in laser equations.
"""
function vecB(
    N::AbstractVector,
    P::AbstractVector,
    Gamma::AbstractMatrix,
    dI::AbstractVector,
    dGamma::AbstractMatrix,
    Q::Dict,
)
    Ncav = length(N)
    Nmode = length(P)
    V = typeof(Q[:V]) <: AbstractVector ? Q[:V] : fill(Q[:V], Ncav)
    f = eltype(dI).types[1] <: Complex || eltype(dGamma) <: Complex ? 0 * im : 0
    B = zeros(eltype(1.0u"Hz"), sum(size(Gamma))) .* f
    Pcav = [sum(Gamma[icav, :] .* P) for icav = 1:Ncav]
    B[1:Ncav] .=
        Q[:eta_i] .* dI ./ Q[:q] .+ [
            sum(
                Q[:R_spprime] .* V[icav] .* dGamma[icav, :] .-
                Pcav[icav] ./ (Gamma[icav, :] .* Q[:tau_p]) .* dGamma[icav, :],
            ) for icav = 1:Ncav
        ]
    # Calculate photons in cavities
    # PPR term
    # Source: Antti Laakso and Mihail Dumitrescu, "Modified Rate Equation Model Including the Photon-Photon Resonance", NUSOD 2010, Eq 3
    B[(Ncav+1):end] .= [
        sum(
            Q[:R_spprime] .* V .* dGamma[:, imode] .-
            P[imode] ./ (Gamma[:, imode] .* Q[:tau_p]) .* dGamma[:, imode],
        ) for imode = 1:Nmode
    ]
    B
end

# Generate the modulation matrix problem and solve
# Source: Coldren page 261, Eq 5.40

"""
    small_signal_modulation_response(
		N::AbstractVector,
		P::AbstractVector,
		Gamma::AbstractMatrix,
		dI::AbstractVector,
		dGamma::AbstractMatrix,
		omega::Quantity;
		laserconstants::Dict = VCSELQuants,
		Coldren::Bool = true,
	)->(dN, dP)

Calculate the small-signal modulation response. Returns response as
vector of of carrier and photon numbers responses `dN` and `dP`.

# Arguments:
- `N`: DC bias carrier densities in each cavity.
- `P`: DC bias photon numbers in each mode.
- `Gamma`: modal gain-overlap factor (function of time that returns matrix giving overlap of each [Cavity, Mode]).
- `dI`: strength of small-signal modulation in each cavity.
- `dGamma`: strength of photon-photon resonance induced gain-overlap variation (matrix giving overlap of each [Cavity, Mode]).
- `omega`: modulation frequency.
- `laserconstants`: dictionary of various laser constants to be used in laser equations.
- `Coldren`: whether to run in compatibility with assumptions in Coldren and Corzine book.
"""
function small_signal_modulation_response(
    N::AbstractVector,
    P::AbstractVector,
    Gamma::AbstractMatrix,
    dI::AbstractVector,
    dGamma::AbstractMatrix,
    omega::Quantity;
    laserconstants::Dict = VCSELQuants,
    Coldren::Bool = true,
)
    @assert length(dI) == length(N)
    @assert size(Gamma) == size(dGamma) == (length(N), length(P))
    A = matA(N, P, Gamma, laserconstants, Coldren = Coldren)
    B = vecB(N, P, Gamma, dI, dGamma, laserconstants)
    A, B = LinearAlgebra.diagm(omega * im * ones(length(B))) .+ A, B
    A, B = Unitful.ustrip.(u"Hz", A), Unitful.ustrip.(u"Hz", B)
    dNsol = A \ B
    Ncav, Nmode = length(N), length(P)
    dNsol[1:Ncav], dNsol[Ncav.+(1:Nmode)]
end
