import Statistics
using Unitful

"""
    laser_array_steady_state(
		Ncav::Integer,
		Nmode::Integer,
		tmax::Quantity,
		I::AbstractVector,
		Gamma::AbstractMatrix;
		laserconstants::Dict = VCSELQuants,
		difflimit::Number = 1e-2,
		tspan::Quantity = 1u"ns",
		dt::Quantity = 0.1u"ns",
	)->(N, P)

Find the steady-state carrier and photon densities.
Returns an `NLsolve` solution for cavity carrier and
modal photon densities `N` and `P`. Works by simulating evolution of system
for `tmax` time and assumes last `tspan` time is the steady-state.

# Arguments:
- `Ncav`: number of cavities.
- `Nmode`: number of array modes.
- `tmax`: time of simulation.
- `I`: driving currents to each cavity (vector of current values).
- `Gamma`: modal gain-overlap factor (matrix giving overlap of each [Cavity, Mode]).
- `laserconstants`: dictionary of various laser constants to be used in laser equations.
- `difflimit`: targeted maximal relative variation in densities during end of simulation.
- `tspan`: length of time at end of simulation to use to determine "steady-state" values (and variation).
- `dt`: time-step used when analyzing "steady-state" variation.
"""
function laser_array_steady_state(
    Ncav::Integer,
    Nmode::Integer,
    tmax::Quantity,
    I::AbstractVector,
    Gamma::AbstractMatrix;
    laserconstants::Dict = VCSELQuants,
    difflimit::Number = 1e-2,
    tspan::Quantity = 1u"ns",
    dt::Quantity = 0.1u"ns",
)
    @assert tmax > tspan
    sol = laser_array_time_simulation(
        Ncav,
        Nmode,
        tmax,
        t -> I,
        t -> Gamma,
        laserconstants = laserconstants,
    )
    tvals = ustrip.(u"s", tmax .- (0u"s":dt:tspan))
    res = sol.(tvals)
    variation = [-(-(extrema(getindex.(res, i))...)) for i = 1:(Ncav+Nmode)]
    average = [Statistics.mean(getindex.(res, i)) for i = 1:(Ncav+Nmode)]
    if any(variation ./ average .> difflimit)
        @warn string(
            "Variation in some quantities over the last ",
            tspan,
            " exceeds a relative factor of ",
            difflimit,
            ". Steady state may not have been reached.",
        )
    end
    average[1:Ncav], average[Ncav.+(1:Nmode)]
end
