using Unitful
import QuadGK, Calculus

# Source: Antti Laakso and Mihail Dumitrescu, "Modified Rate Equation Model Including the Photon-Photon Resonance", NUSOD 2010, derived from Eq. 6
"""
    deltaGamma(Gamma::Function, omega_mod::Quantity, t_phaselock::Quantity)

Obtain an effective confinement factor variation at a given modulation angular
frequency `omega_mod`, given the time-varying confinement factor function `Gamma`
and the phase-locked coherence time for the PPR modes `t_phaselock`.
"""
function deltaGamma(Gamma::Function, omega_mod::Quantity, t_phaselock::Quantity)
    # Time-derivative of confinement factor
    dGamma(t) = Calculus.derivative(t -> Gamma(t * 1u"s"), ustrip(u"s", t)) * 1u"Hz"
    # Find the effective confinement factor variation over the PPR mode phase-lock time
    #TODO: divide by t_phaselock to have units of Hz?
    dG = QuadGK.quadgk(t -> exp(-im * omega_mod * t) * dGamma(t), 0u"s", t_phaselock)[1]
    ustrip.(u"Hz*s", dG)
end

# Source: Analytical results found using Maxima given Gamma(t)=Gamma_DC+dg*sin(omega_ppr*t)
#TODO: Add files/code for calculating this
"""
    deltaGammaSin(omega_ppr::Quantity, omega_mod::Quantity, t_phaselock::Quantity, dg)

Calculate the effective dGamma for at a given modulation frequency `omega_mod`
given PPR resonance frequency `omega_ppr` and confinement factor variation
amplitude `dg` over a mode-phase-coherence lifetime of `t_phaselock`.
"""
function deltaGammaSin(omega_ppr::Quantity, omega_mod::Quantity, t_phaselock::Quantity, dg)
    omega_ppr * (
        dg * (
            exp(im * t_phaselock * omega_mod) * omega_ppr * sin(t_phaselock * omega_ppr) +
            im *
            omega_mod *
            exp(im * t_phaselock * omega_mod) *
            cos(t_phaselock * omega_ppr) - im * omega_mod
        )
    ) / ((omega_ppr - omega_mod) * (omega_ppr + omega_mod))
end
#TODO: Wrong/outdate? Fix or remove
#=
function deltaGammaSin(
    omega_ppr::Quantity,
    omega_mod::Quantity,
    t_phaselock::Quantity,
    dg,
    phi,
)
    @. (
        dg *
        omega_ppr *
        exp(-im * omega_mod * t_phaselock) *
        (
            omega_ppr * sin(omega_ppr * t_phaselock + phi) -
            im * omega_mod * cos(omega_ppr * t_phaselock + phi) -
            omega_ppr * sin(phi) * exp(im * omega_mod * t_phaselock) +
            im * omega_mod * cos(phi) * exp(im * omega_mod * t_phaselock)
        )
    ) / ((omega_ppr - omega_mod) * (omega_ppr + omega_mod))
end

function deltaGammaSin(
    dGamma,
    phi,
    omega_PPR::Quantity,
    omega_mod::Quantity,
    t_phaselock::Quantity,
)
    # Time-derivative of confinement factor
    omega_PPR = ustrip(u"Hz", omega_PPR)
    omega_mod = ustrip(u"Hz", omega_mod)
    Gamma(t) = @. dGamma * sin(omega_PPR * t + phi)
    dGammaf = Calculus.derivative(Gamma)
    # Find the effective confinement factor variation over the PPR mode phase-lock time
    #TODO: divide by t_phaselock to have units of Hz?
    tend = ustrip(u"s", t_phaselock)
    dG = QuadGK.quadgk(t -> exp(-im * omega_mod * t) .* dGammaf(t), 0, tend)[1]
end
=#
