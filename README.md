# LASERRateEqs

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://pawelstrzebonski.gitlab.io/LASERRateEqs.jl)
[![Build Status](https://gitlab.com/pawelstrzebonski/LASERRateEqs.jl/badges/master/pipeline.svg)](https://gitlab.com/pawelstrzebonski/LASERRateEqs.jl/pipelines)
[![Coverage](https://gitlab.com/pawelstrzebonski/LASERRateEqs.jl/badges/master/coverage.svg)](https://gitlab.com/pawelstrzebonski/LASERRateEqs.jl/commits/master)

## About

`LASERRateEqs.jl` is a Julia package for analyzing lasers and laser arrays
using time-domain photon/carrier rate equations and derived small-signal
analysis.

Note: This is a work-in-progress and may have errors!

## Features

* Time-domain rate equation solving
* Frequency-domain small-signal solving
* Supports multiple modes
* Supports multiple cavities
* Supports photon-photon resonance as time-varying confinement factor

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/LASERRateEqs.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for LASERRateEqs.jl](https://pawelstrzebonski.gitlab.io/LASERRateEqs.jl/).
